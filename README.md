# microservice-base
## Installation
As this package wont be published to NPM, it will require installing directly from bitbucket (which can be done with npm)

either use npm in the command line
```
npm install bitbucket:fnvi/microservice-base
```
or add
```
"microservice-base": "bitbucket:fnvi/microservice-base"
```
to your package.json file and run npm install

## Usage

### Code
The package sets and configures express and required middlewares then returns an express app object which is used like any other express application. To use a route you should send an X-Access-Token header with a signed json web token using the same secret used to run the application (details below of config).Dont forget to set the app to listen
```javascript
var app = require('microservice-base');

// All your routes and other code here, an example is blow to output the user
app.get('/',function(req,res){
  res.json(req.user);
});

app.listen(process.env.port);
```
### CLI
The package also parses command line interface arguments to set variable within your microservice

| shorthand | fullname | Description |
| --- | --- | --- |
| -p | --port | Sets the port to use |
| -s | --secret | Sets the secret to sign json web tokens |
| -d | --mongoURI | Sets the database for the service using mongoURI |

Here's an example that sets the port to 3000, the secret to somesecretstring and the database named test at 127.0.0.1
```
node index.js -p 3000 -s somesecretstring -d "mongodb://127.0.0.1:27017/test"
```
*If using nodemon be sure to include two hyphens between the file and arguments to pass them through*
```
nodemon index.js -- -p 3000 -s somesecretstring -d "mongodb://127.0.0.1:27017/test"
```