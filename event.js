let avro = require('avsc');
let mongoose = require('mongoose');

/**
 * Add type for javascript dates
 */
class DateType extends avro.types.LogicalType {
  _fromValue(val) {
    return new Date(val);
  }
  _toValue(date) {
    return date instanceof Date ? +date : undefined;
  }
  _resolve(type) {
    if (avro.Type.isType(type, 'long', 'string', 'logical:timestamp-millis')) {
      return this._fromValue;
    }
  }
}
/**
 * Add type for mongoose ObjectId
 */
class ObjectIdType extends avro.types.LogicalType {
  _fromValue(val) {
    return new mongoose.Types.ObjectId(val);
  }
  _toValue(objectId) {
    return objectId instanceof mongoose.Types.ObjectId ? objectId.toString() : undefined;
  }
  _resolve(type) {
    if (avro.Type.isType(type, 'string', 'string', 'logical:objectId')) {
      return this._fromValue;
    }
  }
}

/**
 * Message schema
 */
module.exports = avro.Type.forSchema({
    type:'record',
    name: 'action',
    fields: [
        {name:"producer", type:'string'},
        {name:"timestamp", type: {type: 'long', logicalType: 'timestamp-millis'}},
        {name:"_id", type: {type: 'string', logicalType: 'objectId'}}
    ]
}
,{logicalTypes: {'timestamp-millis': DateType, 'objectId':ObjectIdType}}
);