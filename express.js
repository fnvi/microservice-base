var express = require('express');

/**
 * Create app
 */
var app = express();

/**
 * Add required middleware
 */
var bodyParser = require('body-parser');
var morgan = require('morgan');
var helmet = require('helmet');

app.use(helmet());
app.use(bodyParser.json());
if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'));
}

module.exports = app;