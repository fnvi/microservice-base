/**
 * Add environment variables
 */
process.env.port = process.env.port || 3000;
process.env.mongoURI = process.env.mongoURI || "mongodb://localhost:27017/databasename";
process.env.secret = process.env.secret || "somesecretstring";

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(process.env.mongoURI);

/**
 * Export useful things
 */
module.exports = {
    app: require('./express'),
    auth: require('./jwt'),
    kafka: require('./kafka')
};