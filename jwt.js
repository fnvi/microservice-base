var jwt = require('jwt-express');

/**
 * Configure JWT middleware
 */
var auth = new jwt(process.env.secret,'X-Access-Token','10m',null,function(req,payload,next){
    req.user = payload.user;
    req.permissions = payload.permissions;
    next();
});

module.exports = auth;