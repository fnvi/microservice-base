let kafka = require('kafka-node');
let events = require('events');

let messageSchema = require('./event');

module.exports = function(zookeeper,topics){



/**
 * Setup Producer
 */
var client = new kafka.Client(zookeeper);
var producer = new kafka.HighLevelProducer(client);

producer.on('ready',function(){
    producer.createTopics(topics,function(err,data){
        
        if(err)
            console.log('error', err)
        eventEmitter.emit('ready', data);
    });
});

/**
 * Setup Consumer
 */

var options = {
    id: process.env.consumerId,
    host: zookeeper,
    groupId: 'testGroup',
    protocol: ['roundrobin'],
    fromOffset:'earliest',
    encoding: 'buffer'
};

var consumer = new kafka.ConsumerGroup(options,topics);

/**
 * Create event emitter
 */
var eventEmitter = new events.EventEmitter();

/**
 * Wrap ready function
 */
producer.on('ready',function(){
    console.log(topics);
    producer.createTopics(topics,function(err,data){
        if(err)
            console.log('error', err)
        eventEmitter.emit('ready', topics);
    });
});

/**
 * Wrap message function
 */
consumer.on('message', function(message){
        eventEmitter.emit(message.topic, messageSchema.fromBuffer(Buffer(message.value)));
});

/**
 * Wrap error functions
 */
producer.on('error', err => eventEmitter.emit('error',err));
consumer.on('error',err => eventEmitter.emit('error',err));

/**
 * Needs sorting to get schema from topic as well as returning a promise
 * @param {String} topic 
 * @param {Object} message 
 * @return {Promise} An ES6 Promise
 */
function sendMessage(topic, message){
    console.log('Sending topic : ',topic);
    return new Promise((resolve, reject)=>{
        producer.send(
            [
                {
                    topic:topic,
                    messages: messageSchema.toBuffer(message)
                }
            ],
            (err,data)=>{
                 if(err)
                    reject(err);

                resolve(data);
            });
    });
}

function on(event,listener){
    eventEmitter.on(event,listener);
}

process.once('SIGINT', function(){
    console.log("stopping service");
    consumer.close(true,error => console.log('error',error));
});


return {
    producer: producer,
    consumer: consumer,
    emit: sendMessage,
    on: on
}
};


