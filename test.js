var base = require('./index');

app = base.app;

app.use(base.auth.verify());

app.get('/',function(req,res){
    res.json(req.user);
});

app.listen(process.env.port);

module.exports = app;