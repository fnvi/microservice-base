process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../test');
let should = chai.should();

let objects = require('./objects.json');

chai.use(chaiHttp);

describe('/GET Authorisation', ()=>{
    it('should be unauthorised without a header', done =>{
        chai.request(server)
            .get('/')
            .end((err,res) =>{
                res.should.have.status(403);
                res.body.should.be.a('object');
                done();
            });
    });

    it('should be unauthorised with invalid token', done =>{
        chai.request(server)
        .get('/')
        .set('X-Access-Token',objects.JWT.invalid)
        .end((err,res) => {
            res.should.have.status(401);
            res.body.should.be.a('object');
            done();
        });
    });

    it('should be unauthorised with expired token', done =>{
        chai.request(server)
        .get('/')
        .set('X-Access-Token',objects.JWT.expired)
        .end((err,res) => {
            res.should.have.status(401);
            res.body.should.be.a('object');
            done();
        });
    });

    it('should be authorised with a valid token', done =>{
        chai.request(server)
        .get('/')
        .set('X-Access-Token',objects.JWT.valid)
        .end((err,res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            done();
        });
    });
});